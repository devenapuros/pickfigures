package project;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cone;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.pickfast.behaviors.PickRotateBehavior;
import com.sun.j3d.utils.universe.SimpleUniverse;
import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Material;
import javax.media.j3d.PointLight;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsConfiguration;

public class PickFigures extends JPanel
{
    BoundingSphere bounding = new BoundingSphere();

    public PickFigures()
    {
        SimpleUniverse simpleUniverse;
        GraphicsConfiguration graphicsConfiguration;
        Canvas3D canvas3D;
        BranchGroup branchGroup;
        graphicsConfiguration = SimpleUniverse.getPreferredConfiguration();
        canvas3D = new Canvas3D(graphicsConfiguration);
        simpleUniverse = new SimpleUniverse(canvas3D);
        branchGroup = createSceneGraph(canvas3D);
        branchGroup.compile();
        simpleUniverse.getViewingPlatform().setNominalViewingTransform();
        simpleUniverse.addBranchGraph(branchGroup);
        setLayout(new BorderLayout());
        add(canvas3D, BorderLayout.CENTER);
    }

    private BranchGroup createSceneGraph(Canvas3D canvas)
    {
        BranchGroup root = new BranchGroup();
        Background background = createBackground();
        PointLight pointLight = createPointLight1();
        PointLight pointLight2 = createPointLight2();
        AmbientLight ambientLight = createAmbientLight();
        TransformGroup scaleTransformGroup = createScaleTransformGroup();
        Appearance appearance = new Appearance();
        appearance.setMaterial(new Material());
        root.addChild(background);
        root.addChild(pointLight);
        root.addChild(pointLight2);
        root.addChild(ambientLight);

        BoundingSphere behaveBounds = new BoundingSphere();
        TransformGroup objRotate = createBoxTransformGroup(-0f, 0f, -1f,1.0f,1.0f,1.0f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        PickRotateBehavior pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        behaveBounds = new BoundingSphere();
        objRotate = createBoxTransformGroup(0f,2f,-1f,3f,0.3f,0.6f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        behaveBounds = new BoundingSphere();
        objRotate = createCylinderTransformGroup(-2.5f,-1.5f,-1f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        behaveBounds = new BoundingSphere();
        objRotate = createConeTransformGroup(2.5f,-1.5f,-1f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        behaveBounds = new BoundingSphere();
        objRotate = createSphereTransformGroup(3f,-1f,2f,0.5f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        behaveBounds = new BoundingSphere();
        objRotate = createCylinderTransformGroup(0f,-3f,-4f,2f,1f);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        scaleTransformGroup.addChild(objRotate);
        pickRotate = new PickRotateBehavior(root,canvas,behaveBounds);
        scaleTransformGroup.addChild(pickRotate);

        root.addChild(scaleTransformGroup);
        return root;
    }

    private Background createBackground()
    {
        Background background = new Background();
        background.setApplicationBounds(bounding);
        return background;
    }

    private PointLight createPointLight1()
    {
        PointLight light = new PointLight(new Color3f(Color.green), new Point3f(3f,3f,3f), new Point3f(1f,0f,0f));
        light.setInfluencingBounds(bounding);
        return light;
    }

    private PointLight createPointLight2()
    {
        PointLight light = new PointLight(new Color3f(Color.orange), new Point3f(-2f,2f,2f), new Point3f(1f,0f,0f));
        light.setInfluencingBounds(bounding);
        return light;
    }

    private AmbientLight createAmbientLight()
    {
        AmbientLight light = new AmbientLight(true, new Color3f(Color.red));
        BoundingSphere bounds;
        light.setInfluencingBounds(bounding);
        return light;
    }

    private TransformGroup createScaleTransformGroup()
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setScale(0.25);
        TransformGroup tG = new TransformGroup(transform3D);
        return tG;
    }

    private TransformGroup createSphereTransformGroup()
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(0f,1.5f,0f));
        TransformGroup tG = new TransformGroup(transform3D);
        Sphere sphere = new Sphere();
        tG.addChild(sphere);
        return tG;
    }
    private TransformGroup createSphereTransformGroup(float x,float y,float z,float radius)
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Sphere sphere = new Sphere(radius);
        tG.addChild(sphere);
        return tG;
    }

    private TransformGroup createConeTransformGroup(float x,float y,float z)
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Cone cone = new Cone();
        tG.addChild(cone);
        return tG;
    }

    private TransformGroup createConeTransformGroup(float x,float y,float z,float radius,float height)
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Cone cone = new Cone(radius,height);
        tG.addChild(cone);
        return tG;
    }

    private TransformGroup createCylinderTransformGroup(float x,float y,float z)
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Cylinder cylinder = new Cylinder();
        tG.addChild(cylinder);
        return tG;
    }

    private TransformGroup createCylinderTransformGroup(float x,float y,float z,float radius,float height)
    {
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Cylinder cylinder = new Cylinder(radius,height);
        tG.addChild(cylinder);
        return tG;
    }

    private TransformGroup createBoxTransformGroup(float x,float y,float z,float xdim,float ydim,float zdim)
    {
        Appearance appearance = new Appearance();
        appearance.setMaterial(new Material());
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3f(x,y,z));
        TransformGroup tG = new TransformGroup(transform3D);
        Box box = new Box(xdim,ydim,zdim,appearance);
        tG.addChild(box);
        return tG;
    }

    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        frame.add(new PickFigures());
        frame.setSize(800,800);
        frame.setTitle("Pick Figures");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
